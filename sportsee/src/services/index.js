import axios from "axios";

export const getUserInformation = async (id_user) => {
    return await axios.get(`http://localhost:3000/user/${id_user}`);
}

export const getUserActivity = async (id_user) => {
    return await axios.get(`http://localhost:3000/user/${id_user}/activity`);
}

export const getUserAverageSessions = async (id_user) => {
    return await axios.get(`http://localhost:3000/user/${id_user}/average-sessions`);
}

export const getUserPerformance = async (id_user) => {
    return await axios.get(`http://localhost:3000/user/${id_user}/performance`);
}