import './Sidebar.css';
import iconYoga from './../../assets/icon_yoga.svg';
import iconNatation from './../../assets/icon_natation.svg';
import iconVelo from './../../assets/icon_velo.svg';
import iconMuscu from './../../assets/icon_muscu.svg';

function Sidebar() {
    return (
        <div className="sidebar">
            <div className="icons">
                <img src={iconYoga} altt="Icon Yoga" />
                <img src={iconNatation} altt="Icon Yoga" />
                <img src={iconVelo} altt="Icon Yoga" />
                <img src={iconMuscu} altt="Icon Yoga" />
            </div>
            <div className="copyright">
                Copiryght, SportSee 2020
            </div>
        </div>
    );
}
export default Sidebar;