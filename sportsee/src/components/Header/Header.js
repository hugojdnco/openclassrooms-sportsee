import Nav from '../Nav/Nav';
import Sidebar from '../Sidebar/Sidebar';
import './Header.css';

function Header() {
    return (
        <header>
            <Nav />
            <Sidebar />
        </header>
    );
}
export default Header;