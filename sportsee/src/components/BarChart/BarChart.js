import './BarChart.css';
import { getUserActivity } from '../../services';
import { useState, useEffect } from 'react';
import { LineChart, Line, BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';

function BarChartComponent({ id_user }) {
    const [userActivity, setUserActivity] = useState();
    let dataActivity = [];

    function formatDataActivity() {
        userActivity && userActivity.data.sessions.forEach((session, index) => {
            dataActivity.push({ name: session.day.slice(-2), poids: session.kilogram, calories: session.calories });
        });
    }

    useEffect(() => {
        getUserActivity(id_user).then((res) => setUserActivity(res.data));
    })

    formatDataActivity();


    return (
        <ResponsiveContainer width="100%" height="100%">
            <BarChart
                width={500}
                height={300}
                data={dataActivity}
                margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}
                barGap={8}
                barCategoryGap="35%"
            >
                <CartesianGrid strokeDasharray="3 3" vertical={false} />
                <XAxis dataKey="name" />
                <YAxis dataKey="calories" orientation='right' tickCount={3} tickSize="0" domain={[0, 'auto']} type='number' interval="preserveStartEnd" axisLine={false}
                    tickLine={false} />
                <Tooltip />
                <Legend />
                <Bar dataKey="poids" fill="#282D30" radius={[25, 25, 0, 0]} />
                <Bar dataKey="calories" fill="#E60000" radius={[25, 25, 0, 0]} />
            </BarChart>
        </ResponsiveContainer>
    )
}
export default BarChartComponent;