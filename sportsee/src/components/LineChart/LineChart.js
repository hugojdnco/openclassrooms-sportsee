import './LineChart.css';
import { LineChart, Line, BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts';
import { getUserAverageSessions } from '../../services';
import { useState, useEffect } from 'react';

function LineChartComponent({ id_user }) {
    let dataAverageSession = [];
    const [userAverageSessions, setUserAverageSessions] = useState();

    function formatDataAverageSession() {
        const day = ['L', 'M', 'M', 'J', 'V', 'S', 'D'];
        userAverageSessions && userAverageSessions.data.sessions.forEach((session, index) => {
            dataAverageSession.push({ name: day[index], sessionLength: session.sessionLength });
        });
    }

    useEffect(() => {
        getUserAverageSessions(id_user).then((res) => setUserAverageSessions(res.data));
    })

    formatDataAverageSession();

    function CustomTooltip({ active, payload }) {
        if (active && payload) {
            return `${payload[0].value} min`;
        }

        return null;
    }
    return (
        <ResponsiveContainer width="100%" height="100%">
            <LineChart
                data={dataAverageSession}
                outerRadius="75%"
                margin={{ top: 0, right: 12, bottom: 24, left: 12 }}
                className="lineChart"
            >
                <XAxis
                    dataKey="name"
                    stroke="rgba(255, 255, 255, 0.6)"
                    axisLine={false}
                    tickLine={false}
                    tick={{
                        fontSize: 12,
                        fontWeight: 500,
                    }}
                />
                <YAxis
                    dataKey="sessionLength"
                    domain={[0, "dataMax + 60"]}
                    hide={true}
                />
                <Line
                    dataKey="sessionLength"
                    type="monotone"
                    stroke="rgba(255, 255, 255, 0.6)"
                    strokeWidth={2}
                    dot={false}
                    activeDot={{
                        stroke: "rgba(255,255,255, 1)",
                        strokeWidth: 5,
                        r: 5,
                    }}
                />
                <Tooltip
                    content={<CustomTooltip />}
                    cursor={{
                        stroke: "rgba(0, 0, 0, 0.1)",
                        strokeWidth: 100,
                    }}
                />
            </LineChart>
        </ResponsiveContainer>
    )
}
export default LineChartComponent;