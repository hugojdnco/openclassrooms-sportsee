import './RadaChart.css';
import { useState, useEffect } from 'react';
import { getUserPerformance } from '../../services';
import { Radar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, ResponsiveContainer } from 'recharts';

function RadarChartComponent({ id_user }) {
    const [userPerformance, setUserPerformance] = useState();
    let dataPerformance = [];

    function formatDataPerformance() {
        userPerformance && userPerformance.data.data.forEach((data, index) => {
            dataPerformance.push({ 'subject': userPerformance && userPerformance.data.kind[data.kind], 'A': data.value })
        });
    }

    useEffect(() => {
        getUserPerformance(id_user).then((res) => setUserPerformance(res.data));
    })

    formatDataPerformance();
    return (
        <ResponsiveContainer width="100%" height="100%">
            <RadarChart
                outerRadius="60%"
                data={dataPerformance}
                className="radarChart">
                <PolarGrid
                    radialLines={false}
                />
                <PolarAngleAxis
                    dataKey="subject"
                    stroke="white"
                    tickLine={false}
                    tick={{
                        fontSize: 12,
                        fontWeight: 500,
                    }} />
                <PolarRadiusAxis tick={false} axisLine={false} />
                <Radar
                    dataKey="A"
                    fill="#FF0101"
                    fillOpacity={0.7}
                    stroke="transparent" />
            </RadarChart>
        </ResponsiveContainer>
    );
}
export default RadarChartComponent;