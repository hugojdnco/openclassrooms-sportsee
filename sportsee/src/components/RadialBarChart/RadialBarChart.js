import './RadialBarChart.css';
import { useState, useEffect } from 'react';
import { getUserInformation } from '../../services';
import { RadialBarChart, RadialBar, Legend, ResponsiveContainer, Tooltip, PolarAngleAxis } from 'recharts';

function RadialBarChartComponent({ userInformation, id_user }) {
    console.log(userInformation && userInformation.data.todayScore)
    const data = [
        {
            uv: userInformation && userInformation.data.todayScore * 100,
            fill: '#8884d8',
        },
    ];

    return (
        <ResponsiveContainer width="100%" height="100%">
            <RadialBarChart
                cx="50%"
                cy="50%"
                innerRadius="70%"
                data={data}
                startAngle={90}
                endAngle={-270}
            >
                <PolarAngleAxis type="number" domain={[0, 100]} angleAxisId={0} tick={false} />
                <RadialBar
                    background
                    cornerRadius={25}
                    dataKey="uv"
                    fill='#FF0000'
                />
            </RadialBarChart>
        </ResponsiveContainer>
    );
}
export default RadialBarChartComponent;