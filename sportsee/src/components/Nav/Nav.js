import './Nav.css';
import logo from './../../assets/logo.png';

function Nav() {
    return (
        <nav>
            <div className="logo">
                <img src={logo} alt="Logo SportSee" />
            </div>
            <div className="navBar">
                <ul>
                    <li><a href="/">Accueil</a></li>
                    <li><a href="#">Profil</a></li>
                    <li><a href="#">Réglage</a></li>
                    <li><a href="#">Communauté</a></li>
                </ul>
            </div>
        </nav>
    );
};
export default Nav;