import Header from '../../components/Header/Header';
import './Home.css';
import iconCalories from './../../assets/calories-icon.svg';
import iconProtein from './../../assets/protein-icon.svg';
import iconGlucides from './../../assets/carbs-icon.svg';
import iconLipides from './../../assets/fat-icon.svg';
import { useState, useEffect } from 'react';
import { getUserInformation } from '../../services';
import BarChartComponent from './../../components/BarChart/BarChart';
import LineChartComponent from './../../components/LineChart/LineChart';
import RadarChartComponent from '../../components/RadarChart/RadarChart';
import RadialBarChartComponent from '../../components/RadialBarChart/RadialBarChart';


function Home() {
    const [userInformations, setUserInformations] = useState();

    const id_user = 12;

    useEffect(() => {
        getUserInformation(id_user).then((res) => setUserInformations(res.data));
    }, [id_user]);

    return (
        <div>
            <Header />
            <div className="container">
                <h1>Bonjour <span>{userInformations && userInformations.data.userInfos.firstName}</span></h1>
                <p>Félicitation ! Vous avez explosé vos objectifs hier 👏</p>
                <div className="blocCharts">
                    <div className="charts">
                        <div className="chart" id="chartVertical">
                            <BarChartComponent id_user={id_user} />
                        </div>
                        <div className="chart" id="chartVague">
                            <LineChartComponent id_user={id_user} />
                        </div>
                        <div className="chart" id="chartPolygone">
                            <RadarChartComponent id_user={id_user} />
                        </div>
                        <div className="chart" id="chartCircle">
                            <RadialBarChartComponent userInformation={userInformations && userInformations} id_user={id_user} />
                        </div>
                    </div>
                    <div className="listInfos">
                        <div className="stat">
                            <div className="icon">
                                <img src={iconCalories} alt="Icon Calories" />
                            </div>
                            <div className="infos">
                                <h2>{userInformations && userInformations.data.keyData.calorieCount}kCal</h2>
                                <p>Calories</p>
                            </div>
                        </div>
                        <div className="stat">
                            <div className="icon">
                                <img src={iconProtein} alt="Icon Protéines" />
                            </div>
                            <div className="infos">
                                <h2>{userInformations && userInformations.data.keyData.proteinCount}g</h2>
                                <p>Protéines</p>
                            </div>
                        </div>
                        <div className="stat">
                            <div className="icon">
                                <img src={iconGlucides} alt="Icon Glucides" />
                            </div>
                            <div className="infos">
                                <h2>{userInformations && userInformations.data.keyData.carbohydrateCount}g</h2>
                                <p>Glucides</p>
                            </div>
                        </div>
                        <div className="stat">
                            <div className="icon">
                                <img src={iconLipides} alt="Icon Lipides" />
                            </div>
                            <div className="infos">
                                <h2>{userInformations && userInformations.data.keyData.lipidCount}g</h2>
                                <p>Lipides</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    );
}
export default Home;